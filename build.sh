#!/bin/bash -xe
node_version=$1
NVM_VERSION=v0.39.0
IMAGE_NAME=$CI_REGISTRY/nonsequitur/cloud-build-tools

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build \
     --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
     --build-arg "NODE_VERSION=$node_version" \
     --build-arg "NVM_VERSION=$NVM_VERSION" \
     -t $IMAGE_NAME:node-$node_version-$CI_COMMIT_SHORT_SHA .

docker push $IMAGE_NAME:node-$node_version-$CI_COMMIT_SHORT_SHA
