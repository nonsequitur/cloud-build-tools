#!/bin/bash -xe
node_version=$1
IMAGE_NAME=$CI_REGISTRY/nonsequitur/cloud-build-tools
RUN="docker run --rm -i $IMAGE_NAME"
VALIDATIONS=(
  "node --version"
  "npm --version"
  "aws --version"
  "sam --version"
  "docker --version"
)

echo "Validating all software installed:"

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
for validation in "${VALIDATIONS[@]}"
do
  ${RUN}:node-$node_version-$CI_COMMIT_SHORT_SHA $validation
done

