FROM ubuntu:21.04 

ARG NODE_VERSION
ARG NVM_VERSION
ARG BUILD_DATE

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
      && apt install -y \
        bash \
        curl \
        docker \
        gcc \
        python3 \
        python3-dev \
        python3-pip 
RUN touch /root/.bashrc \
      && pip3 install --upgrade pip \
      && pip3 install --no-cache-dir --upgrade \
        awscli \
        aws-sam-cli 
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash 
RUN /bin/bash -cl "source ~/.nvm/nvm.sh && nvm install -s v${NODE_VERSION}"

ENTRYPOINT bash -c "source /root/.bashrc"
