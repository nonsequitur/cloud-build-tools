node_versions := 14 16 17
.PHONY: all ${node_versions} 

all: ${node_versions} 
	@echo "$@ node versions built"

${node_versions}: %: publish-%  
	@echo "buildchain for version $* completed"

build-%:
	@echo "building version $*"
	bash build.sh $*

test-%: build-%
	@echo "testing version $*"
	bash test.sh $*

test-only-%:
	@echo "testing version $*"
	bash test.sh $*

publish-%: test-%
	@echo "publishing version $*"
	bash publish.sh $*

publish-only-%:
	@echo "publishing version $*"
	bash publish.sh $*
