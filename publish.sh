#!/bin/bash -xe
node_version=$1
IMAGE_NAME=$CI_REGISTRY/nonsequitur/cloud-build-tools

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull $IMAGE_NAME:node-$node_version-$CI_COMMIT_SHORT_SHA
docker tag $IMAGE_NAME:node-$node_version-$CI_COMMIT_SHORT_SHA $IMAGE_NAME:node-$node_version

docker push $IMAGE_NAME:node-$node_version
